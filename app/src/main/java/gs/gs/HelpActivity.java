package gs.gs;

import android.os.Bundle;
import android.os.Message;
import android.widget.NumberPicker;

/**
 * Created by nlogn on 2015-11-25.
 */
public class HelpActivity extends GSActivity {

    private NumberPicker weightPicker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help_main);
        weightPicker = (NumberPicker)findViewById(R.id.help_weight);
        weightPicker.setMinValue(0);
        weightPicker.setMaxValue(100);
        weightPicker.setWrapSelectorWheel(false);
    }

    @Override
    public void onMessage(Message msg) {
    }

}
