package gs.gs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;


public class MainActivity extends GSActivity {

    public static Context mContext;

    private ArrayList<MenuListItem> menuList;
    private MenuListViewAdapter menuAdapter;
    private ListView menuListView;
    private String rid;
    private String senderId;

    private class MenuListItem
    {
        public String title;
        public String hint;
        MenuListItem(String title, String hint)
        {
            this.title = title;
            this.hint = hint;
        }
    }

    private class MenuListItemView
    {
        public TextView titleTextView;
        public TextView hintTextView;
    }
    private class MenuListViewAdapter extends BaseAdapter
    {
        private Context context = null;
        public MenuListViewAdapter(Context context)
        {
            super();
            this.context = context;
        }
        @Override
        public int getCount() {
            return menuList.size();
        }

        @Override
        public Object getItem(int position) {
            return menuList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            MenuListItemView itemView;
            if(convertView == null)
            {
                itemView = new MenuListItemView();
                LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.menu_listview_item,null);
                itemView.titleTextView = (TextView)convertView.findViewById(R.id.menu_listView_Title);
                itemView.hintTextView = (TextView)convertView.findViewById(R.id.menu_listView_Hint);

                convertView.setTag(itemView);
            }
            else
            {
                itemView = (MenuListItemView)convertView.getTag();
            }

            MenuListItem menuListItem = menuList.get(position);
            itemView.titleTextView.setText(menuListItem.title);
            itemView.hintTextView.setText(menuListItem.hint);

            return convertView;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        senderId = getString(R.string.senderID);

        mContext = this;

        menuList = new ArrayList<MenuListItem>();
        menuList.add(new MenuListItem(getString(R.string.menu_setting_time),getString(R.string.menu_setting_time_hint)));
        menuList.add(new MenuListItem(getString(R.string.menu_status_check),getString(R.string.menu_status_check_hint)));
        menuList.add(new MenuListItem(getString(R.string.menu_take_a_picture),getString(R.string.menu_take_a_picture_hint)));
        menuList.add(new MenuListItem(getString(R.string.menu_instant_feeding),getString(R.string.menu_instant_feeding_hint)));
        //menuList.add(new MenuListItem(getString(R.string.menu_help),getString(R.string.menu_help_hint)));
        menuList.add(new MenuListItem(getString(R.string.menu_connection_check),getString(R.string.menu_connection_check_hint)));


        menuAdapter = new MenuListViewAdapter(this);
        menuAdapter.notifyDataSetChanged();

        menuListView = (ListView)findViewById(R.id.menu_listView);
        menuListView.setAdapter(menuAdapter);
        menuListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        menuListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MenuListItem menu = (MenuListItem)menuAdapter.getItem(position);
                if (menu.title == getString(R.string.menu_setting_time)) // 배식 설정
                {
                    Intent intent = new Intent(MainActivity.this, PQActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    overridePendingTransition(0, 0);
                    startActivity(intent);
                }
                else if (menu.title == getString(R.string.menu_status_check)) // 사료통, 밥그릇 잔량
                {
                    Intent intent = new Intent(MainActivity.this, StatusActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    overridePendingTransition(0, 0);
                    startActivity(intent);
                }
                else if(menu.title == getString(R.string.menu_take_a_picture))
                {
                    ServerSendThread thread = new ServerSendThread(Server.MSG_ISTHERE);
                    thread.start();
                    progressShow("사진을 찍는 중 입니다.");

                }
                else if(menu.title == getString(R.string.menu_instant_feeding))
                {
                    ServerSendThread thread = new ServerSendThread(Server.MSG_INSTANT);
                    thread.start();
                }

                else if (menu.title == getString(R.string.menu_connection_check)) {

                    ServerSendThread thread = new ServerSendThread(Server.MSG_CHECK);
                    thread.start();
                }
                else if(menu.title == getString(R.string.menu_help))
                {
                    Intent intent = new Intent(MainActivity.this, HelpActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    overridePendingTransition(0, 0);
                    startActivity(intent);
                }
            }
        });
    }
    @Override
    public void onMessage(Message msg)
    {
        switch(msg.what)
        {
            case Server.MSG_CHECK:
                toastShow("eCube와 연결 테스트에 성공하였습니다.");
                break;
            case Server.MSG_ISTHERE:
                if(msg.arg1 == 1)
                {
                    toastShow("애완동물 발견! 사진을 찍습니다.");

                }
                else toastShow("애완동물이 근처에 없습니다.");
                //progressDismiss();
                ServerSendThread thread = new ServerSendThread(Server.MSG_PICTURE);
                thread.start();
                break;
            case Server.MSG_INSTANT:
                if(msg.arg1 == 1)
                {
                    toastShow("바로 밥주기에 성공하였습니다.");
                }
                else toastShow("바로 밥주기에 실패하였습니다.");
                break;
            case Server.MSG_PICTURE:
                progressDismiss();
                String filename = (String)msg.obj;
                String sdPath = Environment.getExternalStorageDirectory().getAbsolutePath();
                File file = new File(sdPath,filename);
                String dir = file.getAbsolutePath();
                Bitmap bm = BitmapFactory.decodeFile(dir);
                Dialog dialog = new Dialog(MainActivity.this);
                System.out.println("dir : " + dir);
                dialog.setTitle("사랑스러운 사진");
                dialog.setContentView(R.layout.picture_item);
                ImageView imgView = (ImageView)dialog.findViewById(R.id.imageView);
                imgView.setImageBitmap(bm);
                dialog.show();
                break;
        }
    }
    // GCM methods...
    void regist(){
        new AsyncTask<Void,Void,String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";


                try {
                    InstanceID instanceID = InstanceID.getInstance(getApplicationContext());
                    rid = instanceID.getToken(senderId, GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    //instanceID.deleteToken(rid,GoogleCloudMessaging.INSTANCE_ID_SCOPE);
                    msg = "Device registered, registration ID=" + rid;

                    //발급받은 토큰을 server로 저장하는 함수 (미구현)
                    //RegistToServer();

                    //발급 받은 토큰을 Sharedpreference로 저장 (미구현)
                    //storeRegistrationId(context, regId);


                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }
        }.execute(null, null, null);
    }

    void unregist()
    {
        new AsyncTask<Void,Void,String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    InstanceID instanceID = InstanceID.getInstance(getApplicationContext());
                    instanceID.deleteInstanceID();
                    msg = "Device unRegistered";

                    //server에 토큰 삭제 요청 (미구현)
                    //unRegistToServer();

                    //Sharedpreference로 저장된 토큰 제거 (미구현)
                    //deleteRegistrationId(context, regId);


                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }
        }.execute(null, null, null);
    }
}
