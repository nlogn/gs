package gs.gs;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.net.Socket;

/**
 * Created by nlogn on 2015-11-13.
 */
abstract public class GSActivity extends AppCompatActivity {
    private Toast toast;
    protected ProgressDialog progressDialog;
    public void progressShow(String message) {
        progressDialog = new ProgressDialog(this,ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(message);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(true);
        progressDialog.show();
    }
    public void progressDismiss() {
        progressDialog.dismiss();
    }
    /*
    private static Typeface mTypeface;
    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);

        if (GSActivity.mTypeface == null)
            GSActivity.mTypeface = Typeface.createFromAsset(getAssets(), "gs.ttf");

        ViewGroup root = (ViewGroup) findViewById(android.R.id.content);
        setGlobalFont(root);
    }
    void setGlobalFont(ViewGroup root) {
        for (int i = 0; i < root.getChildCount(); i++) {
            View child = root.getChildAt(i);
            if (child instanceof TextView)
                ((TextView)child).setTypeface(mTypeface);
            else if (child instanceof ViewGroup)
                setGlobalFont((ViewGroup)child);
        }
    }
    */


    public void toastShow(String msg)
    {
        if( toast != null ) toast.cancel();
            toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT);
        toast.show();
    }
    abstract public void onMessage(Message msg);
    public void setServerSocket(Socket conn)
    {
        GSServerSocket.setServerSocket(conn);
    }
    public void finish()
    {
        super.finish();
        overridePendingTransition(0, 0);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
}
