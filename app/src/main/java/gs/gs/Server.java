package gs.gs;

import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class Server
{
    public static final String SERVER_IP = "54.92.36.252";
    public static final int SERVER_PORT = 8888;

    // MSG
    public static final int MSG_SUBMIT = 1;
    public static final int MSG_CONN = 500;
    public static final int MSG_CONNERROR = 442;
    public static final int MSG_ERROR = 443;

    public static final int MSG_CHECK = 100;
    public static final int MSG_STATE = 110;
    public static final int MSG_RQPQ = 120;
    public static final int MSG_ADDPQ = 121;
    public static final int MSG_RMVPQ = 122;
    public static final int MSG_INSTANT = 130;
    public static final int MSG_ISTHERE = 140;
    public static final int MSG_PICTURE = 150;
    public static final int MSG_PASSWD = 200;

    public static final int MSG_PASSWDERROR = 201;

    public static final int MSG_ECUBEERROR = 444;
}

abstract class ServerThread extends Thread implements Runnable {
    protected Handler msgHandler;
}

class ServerConnThread extends ServerThread
{
    public ServerConnThread()
    {
        msgHandler = ServerMsgHandler.getHandler();
    }
    @Override
    public void run()
    {
        Message msg = new Message();
        try {
            Socket s = new Socket(Server.SERVER_IP, Server.SERVER_PORT); // 54.92.36.252

            DataOutputStream os = new DataOutputStream(s.getOutputStream());
            os.writeInt(Server.MSG_SUBMIT); // submit

            msg.what = Server.MSG_CONN;
            msg.obj = s;
        }
        catch (Exception e)
        {
            msg.what = Server.MSG_CONNERROR;
            msg.obj=null;
        }
        msgHandler.sendMessage(msg);

    }
}

class ServerSendThread extends ServerThread
{
    private int header;
    private int arg1;
    private int arg2;
    private int argCount;
    private Socket conn;
    private byte[] buffer;

    public ServerSendThread(int header)
    {
        this.conn = GSServerSocket.getServerSocket();
        this.header = header;
        this.msgHandler = ServerMsgHandler.getHandler();
        argCount = 0;
        // buffer = new byte[1024];
    }

    @Override
    public void run()
    {
        try {
            if( conn!=null && conn.isConnected() ) {

                synchronized (this)
                {
                    DataOutputStream os = new DataOutputStream(conn.getOutputStream());
                    System.out.println("header : " + header);
                    os.writeInt(header);
                    if(argCount > 0)
                    {
                        System.out.println("arg1 : " + arg1);
                        os.writeInt(arg1);

                    }
                    if(argCount > 1)
                    {
                        System.out.println("arg2 : " + arg2);
                        os.writeInt(arg2);
                    }
                }
            }
            else throw new IOException();

        }
        catch (IOException e) {

            Message msg = new Message();
            msg.what = Server.MSG_ERROR;
            msgHandler.sendMessage(msg);
            e.printStackTrace();
        }
    }

    void setArg1(int arg)
    {
        arg1 = arg;
        argCount = 1;
    }
    void setArg2(int arg)
    {
        arg2 = arg;
        argCount = 2;
    }
}

class ServerRecvThread extends ServerThread
{
    private byte[] buffer;
    private Socket conn;
    private Handler msgHandler;
    public ServerRecvThread() {
        msgHandler = ServerMsgHandler.getHandler();
        this.conn = GSServerSocket.getServerSocket();
        buffer = new byte[10241];
    }
    @Override
    public void run()
    {
        try {

                DataInputStream is = new DataInputStream(conn.getInputStream());
                while(conn.isConnected()) {
                    int header = is.readInt();
                    Message msg = new Message();
                    msg.what = header;
                    System.out.println("recv header : " + header);
                    switch (header) {
                        case Server.MSG_PASSWD:
                            int isCorrect = is.readInt();
                            System.out.println("recv isCorrect : " + isCorrect);
                            if (isCorrect != 1) msg.what = Server.MSG_PASSWDERROR;
                            break;

                        case Server.MSG_RQPQ:

                            GSInfo.pqList.clear();
                            int n = is.readInt();
                            System.out.println("recv PQ size : " + n);
                            for (int i = 0; i < n; i++) {
                                int p = is.readInt();
                                int q = is.readInt();
                                System.out.println("recv P : " + p);
                                System.out.println("recv Q : " + q);
                                GSInfo.pqList.add(new GSInfo.PQ(p, q));
                            }
                            break;

                        case Server.MSG_STATE:
                            int feedBox = is.readInt();
                            int feedBowl = is.readInt();
                            System.out.println("feedBox : " + feedBox);
                            System.out.println("feedBowl : " + feedBowl);
                            GSInfo.feedBox = feedBox;
                            GSInfo.feedBowl = feedBowl;
                            break;

                        case Server.MSG_SUBMIT:
                            int time = is.readInt();
                            msg.arg1 = time;
                            break;

                        case Server.MSG_ISTHERE:
                            int isThere = is.readInt();
                            System.out.println("recv isThere : " + isThere);
                            msg.arg1 = isThere;
                            break;

                        case Server.MSG_INSTANT:
                            int isSuccess = is.readInt();
                            msg.arg1 = isSuccess;
                            break;

                        case Server.MSG_PICTURE:
                            int pictureBytes = is.readInt();
                            System.out.println("file size : " + pictureBytes);
                            int sumBytes = 0;
                            String filename = (new java.text.SimpleDateFormat("yyyyMMdd-HHmmss").format(new java.util.Date())) + ".bmp";
                            String sdPath = Environment.getExternalStorageDirectory().getAbsolutePath();
                            File file = new File(sdPath,filename);
                            FileOutputStream fos = new FileOutputStream(file);
                            while( sumBytes < pictureBytes )
                            {
                                int nRet = is.read(buffer,0,10240);
                                if(nRet < 0)
                                {
                                    System.out.println("end...");
                                    break;
                                }

                                sumBytes += nRet;
                                System.out.println("recv picture : " + sumBytes);

                                fos.write(buffer,0,nRet);
                            }
                            System.out.println("pictureBytes : " + pictureBytes);
                            System.out.println("sumBytes : " + sumBytes);
                            fos.close();
                            msg.obj = filename;
                            break;
                    }
                    msgHandler.sendMessage(msg);
                }
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

}

class ServerMsgHandler extends Handler
{
    private static ServerMsgHandler h;
    private ServerMsgHandler() {}
    public static ServerMsgHandler getHandler()
    {
        if( h == null ) h = new ServerMsgHandler();
        return h;
    }

    @Override
    public void handleMessage(Message msg)
    {
        super.handleMessage(msg);
        switch(msg.what) {
            case Server.MSG_CONN:
            case Server.MSG_CONNERROR:
            case Server.MSG_ERROR:
            case Server.MSG_ECUBEERROR:
            case Server.MSG_SUBMIT:
            case Server.MSG_PASSWD:
            case Server.MSG_PASSWDERROR:
                ((PasswordActivity)PasswordActivity.mContext).onMessage(msg);
                break;

            case Server.MSG_CHECK:
            case Server.MSG_ISTHERE:
            case Server.MSG_INSTANT:
            case Server.MSG_PICTURE:
                ((MainActivity)MainActivity.mContext).onMessage(msg);
                break;


            case Server.MSG_STATE:
                ((StatusActivity)StatusActivity.mContext).onMessage(msg);
                break;


            case Server.MSG_RQPQ:
            case Server.MSG_RMVPQ:
                ((PQActivity)PQActivity.mContext).onMessage(msg);
                break;

            case Server.MSG_ADDPQ:
                ((PQAddActivity)PQAddActivity.mContext).onMessage(msg);
                break;
        }
    }
}