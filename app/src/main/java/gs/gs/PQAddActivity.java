package gs.gs;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;

/**
 * Created by nlogn on 2015-11-23.
 */
public class PQAddActivity extends GSActivity{
    public static Context mContext;


    private Button sendButton;
    private TimePicker periodTimePicker;
    private EditText quantityEditText;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pq_add);
        mContext = this;

        sendButton = (Button)findViewById(R.id.pq_add_sendButton);
        periodTimePicker = (TimePicker) findViewById(R.id.pq_add_period);
        quantityEditText = (EditText) findViewById(R.id.pq_add_quantity);


        sendButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                int hour = periodTimePicker.getCurrentHour();
                int minute = periodTimePicker.getCurrentMinute();
                int period = hour * 60 + minute;
                if( quantityEditText.length() < 1 )
                {
                    toastShow("식사량을 입력해주세요.");
                    return;
                }
                int quantity = Integer.parseInt(quantityEditText.getText().toString());

                ServerSendThread serverSendThread = new ServerSendThread(Server.MSG_ADDPQ);
                serverSendThread.setArg1(period);
                serverSendThread.setArg2(quantity);
                serverSendThread.start();
            }
        });
    }


    @Override
    public void onMessage(Message msg)
    {
        switch(msg.what)
        {
            case Server.MSG_ADDPQ:
                finish();
                break;
        }
    }
}
