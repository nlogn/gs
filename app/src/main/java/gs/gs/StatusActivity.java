package gs.gs;


import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.widget.ProgressBar;
import android.widget.TextView;

public class StatusActivity extends GSActivity
{
    public static Context mContext;

    private ProgressBar feedBoxProgressBar;
    private TextView feedBoxTextView;

    private ProgressBar feedBowlProgressBar;
    private TextView feedBowlTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.status_main);
        mContext = this;

        feedBoxProgressBar = (ProgressBar)findViewById(R.id.status_feedBoxProgressBar);
        feedBoxProgressBar.setProgress(0);
        feedBoxTextView = (TextView)findViewById(R.id.status_feedBoxTextView);
        feedBoxTextView.setText("? %");

        feedBowlProgressBar = (ProgressBar)findViewById(R.id.status_feedBowlProgressBar);
        feedBowlProgressBar.setProgress(0);
        feedBowlTextView = (TextView)findViewById(R.id.status_feedBowlTextView);
        feedBowlTextView.setText("? g");

        // 급식기 상태 받아오기
        toastShow("급식기 상태 정보 요청중...");
        ServerSendThread thread = new ServerSendThread(Server.MSG_STATE);
        thread.start();
    }
    public void refreshStatus()
    {
        int feedBox = GSInfo.feedBox;
        int feedBowl = GSInfo.feedBowl;
        feedBoxProgressBar.setProgress(feedBox);
        feedBoxTextView.setText(feedBox + " %");

        feedBowlProgressBar.setProgress(feedBowl);
        feedBowlTextView.setText(feedBowl + " g");

    }

    @Override
    public void onMessage(Message msg)
    {
        switch(msg.what)
        {
            case Server.MSG_STATE:
                System.out.println("refresh");
                refreshStatus();
                break;
        }
    }

}