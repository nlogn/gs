package gs.gs;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;



public class PQActivity extends GSActivity
{
    public static Context mContext;

    private Button addButton;
    private ListView pqListView;
    private PQListViewAdapter pqListViewAdapter;
    private ArrayList<GSInfo.PQ> pqList;

    private class PQListItemView
    {
        public TextView periodTextView;
        public TextView quantityTextView;
        public Button rmvButton;
    }
    private class PQListViewAdapter extends BaseAdapter
    {
        private Context context = null;
        public PQListViewAdapter(Context context)
        {
            super();
            this.context = context;
        }
        @Override
        public int getCount() {
            return pqList.size();
        }

        @Override
        public Object getItem(int position) {
            return pqList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            PQListItemView itemView;
            if(convertView == null)
            {
                itemView = new PQListItemView();
                LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.pq_listview_item,null);
                itemView.periodTextView = (TextView)convertView.findViewById(R.id.pq_listView_period);
                itemView.quantityTextView = (TextView)convertView.findViewById(R.id.pq_listView_quantity);
                itemView.rmvButton = (Button)convertView.findViewById(R.id.pq_listView_rmvButton);
                convertView.setTag(itemView);
            }
            else
            {
                itemView = (PQListItemView)convertView.getTag();
            }

            final GSInfo.PQ pq = pqList.get(position);
            itemView.periodTextView.setText(GSInfo.PQ.periodToString(pq.getPeriod()));
            itemView.quantityTextView.setText(Integer.toString(pq.getQuantity()) + " g");

            itemView.rmvButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ServerSendThread serverSendThread = new ServerSendThread(Server.MSG_RMVPQ);
                    serverSendThread.setArg1( pq.getPeriod() );
                    serverSendThread.setArg2( pq.getQuantity() );
                    serverSendThread.start();
                }
            });

            return convertView;
        }
    }

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pq_main);

        mContext = this;

        pqList = new ArrayList<GSInfo.PQ>();
        pqListView = (ListView)findViewById(R.id.pq_listView);
        pqListViewAdapter = new PQListViewAdapter(this);
        pqListView.setAdapter(pqListViewAdapter);

        addButton = (Button)findViewById(R.id.pq_addButton);
        addButton.setOnClickListener( new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PQActivity.this, PQAddActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                overridePendingTransition(0, 0);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        // PQ 목록 받아오기
        Toast.makeText(getApplicationContext(),"식사시간 및 식사량 목록 받아오는 중...",Toast.LENGTH_SHORT).show();
        ServerSendThread thread = new ServerSendThread(Server.MSG_RQPQ);
        thread.start();
    }


    public void showPQList()
    {
        pqList = GSInfo.pqList;
        pqListViewAdapter.notifyDataSetChanged();
    }

    @Override
    public void onMessage(Message msg)
    {
        switch(msg.what)
        {
            case Server.MSG_RMVPQ:
                onStart();
                break;
            case Server.MSG_RQPQ:
                showPQList();
                break;
        }
    }
}
