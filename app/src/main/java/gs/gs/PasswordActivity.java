package gs.gs;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;

import java.net.Socket;

/**
 * Created by nlogn on 2015-11-20.
 */
public class PasswordActivity extends GSActivity {

    public static Context mContext;

    private Button submitButton;
    private String password;
    private Switch serverSwitch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.password_main);
        serverSwitch = (Switch)findViewById(R.id.server_switch);
        mContext = this;
        ServerConnThread serverConnThread = new ServerConnThread();
        serverConnThread.start();

        submitButton = (Button)findViewById(R.id.submit_button);
        submitButton.setOnClickListener( new Button.OnClickListener()
        {
            @Override
            public void onClick(View v) {

                EditText passwordEditText = (EditText)findViewById(R.id.password_editText);
                password = passwordEditText.getText().toString();

                if( password.length() != 6 )
                {
                    toastShow("비밀번호 6자리를 입력해주세요.");
                    return;
                }
                ServerSendThread thread = new ServerSendThread(Server.MSG_PASSWD);
                thread.setArg1(Integer.parseInt(password));
                thread.start();
            }
        });
    }
    /*
    public void recvThreadStart()
    {
        ServerRecvThread serverRecvThread = new ServerRecvThread(msgHandler);
        serverRecvThread.start();
    }
    */

    @Override
    public void onMessage(Message msg) {
        switch(msg.what) {
            case Server.MSG_CONN:
                setServerSocket((Socket) msg.obj);
                ServerRecvThread recvThread = new ServerRecvThread();
                recvThread.start();
                toastShow("서버에 접속되었습니다.");
                serverSwitch.setFocusable(false);
                serverSwitch.setClickable(false);
                serverSwitch.setChecked(true);
                break;
            case Server.MSG_CONNERROR:
                toastShow("서버에 접속할 수 없습니다. 서버연결 재시도 요구");
                serverSwitch.setFocusable(false);
                serverSwitch.setClickable(false);
                serverSwitch.setChecked(false);
                break;
            case Server.MSG_ERROR:
                toastShow("서버와 연결이 원활하지 않습니다.");
                break;

            case Server.MSG_ECUBEERROR:
                toastShow("eCube가 서버에 연결되지 않았습니다.");
                break;

            case Server.MSG_SUBMIT:
                toastShow("등록 완료");
                break;

            case Server.MSG_PASSWDERROR:
                toastShow("비밀번호가 틀렸습니다.");
                break;
            case Server.MSG_PASSWD:
                Intent intent = new Intent(PasswordActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                overridePendingTransition(0, 0);
                startActivity(intent);
                break;
        }
    }
}
