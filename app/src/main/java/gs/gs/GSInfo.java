package gs.gs;

import java.net.Socket;
import java.util.ArrayList;

/**
 * Created by nlogn on 2015-11-20.
 */
public class GSInfo {
    public static int serverConnRetry = 0;
    public static boolean isCertificated = false;
    public static class PQ
    {
        public static String periodToString(int period)
        {
            int hour = period/60;
            int minute = period%60;

            return hour + "시 " + minute + "분 ";
        }
        public int getPeriod() {
            return period;
        }
        public int getQuantity() {
            return quantity;
        }

        private int period;
        private int quantity;
        PQ(int p,int q)
        {
            period = p;
            quantity = q;
        }
    }
    public static ArrayList<PQ> pqList = new ArrayList<PQ>();
    public static int feedBox;
    public static int feedBowl;

}

class GSServerSocket
{
    private static Socket serverSocket;
    public static synchronized Socket getServerSocket()
    {
        return GSServerSocket.serverSocket;
    }
    public static synchronized void setServerSocket(Socket s)
    {
        GSServerSocket.serverSocket = s;
    }
}