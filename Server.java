import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Date;

abstract class ClientThread implements Runnable
{
	Server server;
	Socket conn;
	DataInputStream reader;
	byte[] buffer;
	
	public ClientThread(Server server, Socket conn, DataInputStream reader)
	{
		this.buffer = new byte[1024];
		this.server = server;
		this.conn = conn;
		this.reader = reader;
	}
	
	@Override
	public void run() {
		try {
			startThread();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	abstract protected void sendBuffer() throws IOException;
	
	abstract protected void logStart();
	abstract protected void logRecv(int size);
	abstract protected void logEnd();
	
	
	private void startThread() throws IOException
	{
		logStart();
		while( conn.isConnected() )
		{
			try 
			{
				int size = reader.read(buffer);
				if(size > 0)
				{
					logRecv(size);
					sendBuffer();  
				}
			}
			catch(SocketException e)
			{
				break;
			}
		}
		logEnd();
	}
}

class AndroidThread extends ClientThread
{

	public AndroidThread(Server server, Socket conn, DataInputStream reader) {
		super(server, conn, reader);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void sendBuffer() throws IOException {
		// TODO Auto-generated method stub
		server.sendToECube(buffer);
	}


	@Override
	protected void logStart() {
		// TODO Auto-generated method stub
		server.log("Android recv Thread Start");
	}

	@Override
	protected void logRecv(int size) {
		// TODO Auto-generated method stub
		server.log(Integer.toString(size) + " byte(s) recv from Android");
		
	}
	@Override
	protected void logEnd() {
		// TODO Auto-generated method stub
		server.log("Android recv Thread Closed");
		
	}
	
}

class ECubeThread extends ClientThread
{

	public ECubeThread(Server server, Socket conn, DataInputStream reader) {
		super(server, conn, reader);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void sendBuffer() throws IOException {
		// TODO Auto-generated method stub
		server.sendToAndroid(buffer);
		
	}



	@Override
	protected void logStart() {
		// TODO Auto-generated method stub
		server.log("eCube recv Thread Start");
		
	}
	@Override
	protected void logRecv(int size) {
		// TODO Auto-generated method stub
		server.log(Integer.toString(size) + " byte(s) recv from eCube");
	}
	@Override
	protected void logEnd() {
		// TODO Auto-generated method stub
		server.log("eCube recv Thread End");
		
	}
	
}


public class Server {

	public final static int FAIL = 444;
	
	public final static int ANDROID = 1;
	public final static int ECUBE = 2; 
	private ServerSocket server;
	private Socket android;
	private DataOutputStream androidOutputStream;
	private DataInputStream androidInputStream;
	
	private Socket eCube;
	private DataOutputStream eCubeOutputStream;
	private DataInputStream eCubeInputStream;
	public void log(String str)
	{
		Date dt = new Date();
		System.out.println(dt.toString() + ": " + str);
	}
	private int identify(Socket conn) throws IOException
	{
		DataInputStream reader = new DataInputStream(conn.getInputStream());
		/*
		byte[] buffer = new byte[4];
		reader.read(buffer, 0, 4);
		*/
		int buffer, id;
		buffer = reader.readInt();
		id = buffer;
		
		return id;
	}
	public Server()
	{
		Socket conn = null;
		try {
			server = new ServerSocket(8888);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		while(true)
		{ 
			try 
			{
				conn = server.accept();
				log("accepted ip:" + conn.getInetAddress().getHostAddress());
				// identify eCube or Android
				int id = identify(conn);
	
				
				// if identified...
				switch(id)
				{
				case ANDROID:
					if( android != null && android.isConnected() )
					{
						log("Android closed");
						android.close();
					}
					log("Android Connected");
					
					android = conn;
					androidInputStream = new DataInputStream(android.getInputStream());
					androidOutputStream = new DataOutputStream(android.getOutputStream());
					androidOutputStream.writeInt(ANDROID);
					androidOutputStream.writeInt((int) System.currentTimeMillis());
					AndroidThread androidThread = new AndroidThread(this,android,androidInputStream);
					new Thread(androidThread).start();
					break;
				case ECUBE:
					if( eCube != null && eCube.isConnected() ) 
					{
						log("eCube closed");
						eCube.close();
					}
					log("eCube Connected");
					
					eCube = conn;
					eCubeInputStream = new DataInputStream(eCube.getInputStream());
					eCubeOutputStream = new DataOutputStream(eCube.getOutputStream());
					eCubeOutputStream.writeInt(ECUBE);
					eCubeOutputStream.writeInt((int) System.currentTimeMillis());
					ECubeThread eCubeThread = new ECubeThread(this,eCube,eCubeInputStream);
					new Thread(eCubeThread).start();
					break;
				default:
					conn.close();
					log("Unknown id");
					break;
						
				}
			}
			catch (IOException e) {
				// TODO Auto-generated catch block
				log("IOException in server called");
				e.printStackTrace();
			}
			
			
		}
	}
	public void sendToECube(byte[] buffer) throws IOException
	{
		try
		{
			if( eCube!=null && eCube.isConnected() )
			{
				eCubeOutputStream.write(buffer);
				log("sendToECube success");
				return;
			}
			else throw new IOException();
		}
		catch (IOException e)
		{
			androidOutputStream.writeInt(FAIL);
			log("sendToECube fail");
		}
		
	}
	public void sendToAndroid(byte[] buffer) throws IOException
	{
		try
		{
			if( android!=null && android.isConnected() ) 
			{
				androidOutputStream.write(buffer);
				log("sendToAndroid success");
				return;
			}
			else throw new IOException();
		}
		catch(IOException e)
		{
			eCubeOutputStream.writeInt(FAIL);
			log("sendToAndroid fail");
		}
		
	}
	
	
	public static void main(String[] args) {
		Server s = new Server();
	}

}
